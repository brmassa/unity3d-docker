# Unity3D Docker
An Unity3D editor on Ubuntu Linux image which can be used for continuous integration purposes.

## Unity Version
Use the proper tag to specify the version or omit it for the latest version. Examples:

* brunomassa/unity3d-docker _(latest)_
* brunomassa/unity3d-docker:UNITY_5_5_0_B1
* brunomassa/unity3d-docker:UNITY_2017_1_1_B1
* brunomassa/unity3d-docker:UNITY_2017_2_0_F1

All versions that uses DEB files listed in https://forum.unity.com/threads/unity-on-linux-release-notes-and-known-issues.350256 are included.

## Wrapper
In other to run Unity3D in a server, you have to create a display "simulator" called *xvfb*.
So the full command to call Unity3D is

`xvfb-run --error-file /var/log/xvfb_error.log --server-args="-screen 0 1024x768x24" /opt/Unity/Editor/Unity -quit -batchmode -projectPath="" -username="" -password="" etc`

And then include any of the Unity3D arguments (https://docs.unity3d.com/Manual/CommandLineArguments.html), like the `-quit -batchmode`, which are required.
To facilitate, there is a small script called `unity3d-docker.sh`, that includes all these. Use:

`unity3d-docker.sh -projectPath="" -username="" -password="" etc`

## Dockerfile generator
Inside `dockerfile-generator` folder, there is the `unity-versions.sh` with all versions. Modify it and then run `generator.sh` to create a folder with a Docker for each Unity3D version.
After it, you must set it into Docker, to consider these extra folders.
