#!/bin/bash

. /app/unity-versions.sh

# if no version was given, use the latest
if [ $# -eq 0 ]
then
	UNITY_URL=${UNITY_VERSIONS[$UNITY_VERSION_LAST]}
else
	UNITY_URL=${UNITY_VERSIONS[$1]}
fi

# download
echo "Downloading Unity3D... $UNITY_URL"
curl -o /app/unity_editor.deb "$UNITY_URL"
echo "Unity3D downloaded."