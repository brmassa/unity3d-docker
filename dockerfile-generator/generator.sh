#!/bin/bash

# read versions
. unity-versions.sh

function copy {
	version=$1
	# copy all files need
	cp -f ./Dockerfile_template 	../$version/Dockerfile
	cp -f ./unity-versions.sh 	../$version/unity-versions.sh
	cp -f ./unity-download.sh 	../$version/unity-download.sh
	cp -f ./unity-docker.sh 	../$version/unity-docker.sh
}

function replace {
	version=$1
	# inject the Unity3D version name for this Docker
	sed -i -- "s/%URL%/$version/g" ../$version/Dockerfile
}

for version in "${!UNITY_VERSIONS[@]}"
do
	# create the version folder
	if [ ! -d ../$version ]; then
		mkdir ../$version
	fi

	copy $version
	replace $version "../"
done

# Latest
copy ""
replace ""
